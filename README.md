# 捷键 for win 使用说明

基于 [Autohotkey2](https://www.autohotkey.com/) 开发，为简化 Windows 键鼠操作而生的按键映射/快捷键增强工具。既可当作一个传统启动器，又对鼠标侧边按键和热字符支持良好。

强烈建议搭配全局鼠标手势软件 + 带侧边按键的鼠标。

[视频演示](https://www.bilibili.com/video/BV19H4y1e7hJ?vd_source=54168537affc2c02555097cb26797d99) ｜ [软件下载][捷键]

**注意事项：**

1. 若软件功能有差异，以最新版为准。
2. 本程序开源，无毒无后门不收集任何信息。如被误报错杀，可加入安全类软件排除清单。

**和一些软件对比**

当我开发几个月后，发现市场上早就有了此类成熟软件，我可能连其中任一软件的百分之十的完成度都达不到。

| 功能 |  macOS 平台 | Windows 平台 |
| ---- | ---- | ---- |
| 侧边按键支持 | BetterAndBetter【免费】 | [捷键][]【免费】 |
| 热字符串 | Espanso【免费】 | [捷键][]【免费】 |
| 启动器 | Raycast【免费】/Alfred【付费】| [捷键][]【免费】 |
| 启动程序/窗口切换 | hammerspoon【免费】 | [捷键][]【免费】 |

但这毕竟这是我业余摸鱼节假日其他时间个人开发维护，优点怕只剩下功能较多和足够小巧了。这同时指导了我的下一步工作，将逐渐提升交互。尽量做到开箱即用。

## 0. 程序目录结构

1. custom/ 自定义 ahk 函数，用于自定义外部命令
2. extra/【增强体验】WGestures 和 FastGestures 预设手势模版。WindowSpyU32.exe 用于查看窗口标识符。GenerateShortcuts.exe 用于生成快捷方式文件夹 shortcuts。
3. app.csv 配置文件
4. data.csv 配置文件
5. **jiejian32/64.exe** 分别为 32/64 位主程序。无需安装，双击即用，强烈建议设置为开机自启

## 1. 特点

* 开箱即用，支持自定义快捷键，专注按键改写和增强
* 侧边按键支持，推荐搭配带侧边键的鼠标（不区分鼠标厂商）
* 热字符串支持
* 可作为启动器使用，由于定制化程度太高甚至会导致难以上手，提供快捷启动和内置系统命令（锁屏、睡眠、关机等）
* 试验性的加入左键辅助功能

建议搭配免费的全局鼠标手势软件 WGestures 1【免费】/FastGestures【免费】/[WGestures 2【付费】][WGestures 2付费链接]，再说说题外话 mac 平台我觉得 BetterAndBetter【免费】做得很好。

## 2. 热键 之 键鼠操作

1. 按住 CapsLock 后可以用鼠标左键拖动窗口
2. 当鼠标移动到屏幕左边缘或者停留在任务栏时，鼠标滑轮滚动可以调节音量。

![](https://foruda.gitee.com/images/1689318820722473769/d4f9efe3_426858.gif)

| 鼠标 | 按键 | 建议映射手势 | 名称 | 默认用途 | 多标签软件 | 音乐类软件 | 视频类软件 | 看图软件 | 焦点在任务栏 | 焦点在左边界 | 焦点在上边界 | 焦点在桌面 |
| ---- | ---- | ---- |---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| - | Esc | - | 逃逸/esc | 退出窗口 | - | - | - | - | - | - | - | - |
| 鼠标右键 | - | - | - | - | - | - | - | - | - | 播放/暂停 | 播放/暂停 | - |
| 鼠标中键 | - | - | - | - | - | - | - | - | 静音 | 静音 | 静音 | - |
| 滚轮上滑 | - | - | - | - | - | - | - | - | 调高音量 | 调高音量 | 下一曲 | - |
| 滚轮下滑 | - | - | - | - | - | - | - | - | 调低音量 | 调低音量 | 上一曲 | - |
| 鼠标侧边后退键(XB1) | - | - | 关闭/close | 关闭窗口 | 关闭标签 | 关闭窗口 | 关闭窗口 | 关闭窗口 | 下一曲 | 下一曲 | - | - |
| - | Ctrl + F4 | ↑ | 关闭/close | 关闭窗口 | 关闭标签 | 关闭窗口 | 关闭窗口 | 关闭窗口 | - | - | - | - |
| - | Ctrl + F8 | ↓ | 新建/new | 打开或新建文件 | 新建标签 | 打开文件 或 无操作 | 打开文件 或 无操作 | 打开文件 或 无操作 | - | - | - | - |
| - | Alt + ← | ← | 后退/prev | 后退 | 后退 | 上一曲 | 快退 | 上一张 | - | - | - | 上一曲 |
| - | Alt + → | → | 前进/next | 前进 | 前进 | 下一曲 | 快进 | 下一张 | - | - | - | 下一曲 |
| 鼠标侧边前进键(XB2) | - | - | 上一个/prev | - | 上一页 | 上一曲 | 上个视频 | - | 上一曲 | 上一曲 | - | 上一曲 |
| - | Ctrl + Shift + Tab | 上左 | 上一个/prev | - | 上一页 | 上一曲 | 上个视频 | - | - | - | - | - |
| - | Ctrl + Tab | 上右 | 下一个/next | - | 下一页 | 下一曲 | 下个视频 | - | - | - | - | - |
| - | Ctrl + Shift + n | 右下 | 新建窗口/new| 新建窗口 | | | | | | | | |
| - | Ctrl + F7 | 右左 | 置顶/zhiding | 置顶 | | | | | | | | |
| - | F11 | ↓↑ | 全屏/fullscreen | 全屏/取消全屏 | | | | | | | | |

注：

1. 能用鼠标按键尽量用按键，因此最好使用带侧边按键的鼠标。其次考虑使用鼠标手势，最后才考虑使用快捷键。
2. 多标签软件主要为浏览器，支持多标签的文本编辑器、IDE 等。
3. 音乐类软件如 Spotify、QQ 音乐。其中 Ctrl + F3 打开文件对本地音乐播放器适配良好，在线音乐类软件可能不适用。
4. 视频类软件如 PotPlayer、VLC。
5. 看图软件如 2345 看图王、Bandiview。
6. F11 特别适配了 b 站。

以下部分场景使用了鼠标手势软件 WGestures 替代了手动键入快捷键。

操作资源管理器

![](https://foruda.gitee.com/images/1689318878983165049/4F89b32d_426858.gif "动画.gif")

操作 360 极速浏览器

![](https://foruda.gitee.com/images/1689320148290015829/d0563e32_426858.gif "动画.gif")

操作 Jetbrains IDEA

![](https://foruda.gitee.com/images/1689318910813101697/359f150e_426858.gif "动画.gif")

操作 microsoft vscode

![](https://foruda.gitee.com/images/1689318894573526368/39027a0d_426858.gif "动画.gif")

## 3. 热键 之 打开网址

* Alt + 6 打开 B 站
* Alt + 7 打开 IT 之家
* Alt + 8 打开 西瓜视频

![打开网址](https://foruda.gitee.com/images/1689318923398248705/25b1c4c9_426858.gif "动画.gif")

## 4. 热键 之 运行程序

* Alt + 1 打开/切换窗口 资源管理器
* Alt + 2 打开/切换窗口 360 极速浏览器【若程序路径存在 data.csv 中】
* Alt + 3 打开/切换窗口 VSCode【若程序路径存在 data.csv 中】
* Alt + j 打开/切换窗口 记事本

![打开记事本](https://foruda.gitee.com/images/1689318934831690368/2606bf7a_426858.gif "动画.gif")

## 5. 热键 之 启动文件夹

* Alt + d 打开 D 盘

![打开 D 盘](https://foruda.gitee.com/images/1689318944226542670/0337e814_426858.gif "动画.gif")

## 6. 热键 之 其他

预设条件：当 vscode 或 windows 记事本在激活状态下。

* Ctrl + 数字 1-5 为光标所在行添加 markdown 格式标题

![输入图片说明](https://foruda.gitee.com/images/1689318964909077353/0518d03d_426858.gif "动画.gif")

* `Ctrl + Alt + r` 重启脚本
* `Ctrl + Alt + s` 暂停脚本
* `Ctrl + Alt + v` 剪贴板的内容输入到当前活动应用程序中，防止了一些网站禁止在 HTML 密码框中进行粘贴操作
* `Ctrl + Shift + "`  插入一对双引号

## 7. Anyrun 启动器

争做一款简洁、高效的应用启动器。

特点：

1. 无边框设计，界面极简。设计简洁，支持模糊搜索，帮助快速定位应用。
2. 使用普适易读的微软雅黑字体，字号设计合理，字体风格简洁大方。
3. 支持自定义多种关键词进行应用匹配，智能识别用户的启动意图。
4. 系统资源占用少，启动迅速，可靠性高，全天候支持用户的应用启动需求。

使用说明：

* `Alt + 空格` 开启快捷启动器。若再次按下 空格/esc键/鼠标在启动器外点击 则关闭该组件。
* 自动识别剪切板有没有内容，如果输入内容是文件或者网址 且离最后一次 ctrl + c 操作小于 13 秒则自动粘贴内容。
* 支持全拼甚至首字母简拼模糊搜索，上下键切换选中项，回车或直接鼠标双击进行确认。

### 打开程序/文件/网址

在输入框中输入内容后按下回车

* guanyu 打开关于 windows 程序
* fdj 打开放大镜程序
* `163.com` 打开网址-网易网
* `https://www.soso.com` 打开网址-搜搜网
* `D:\code\atomgit\jiejian` 前往文件夹
* `D:\code\atomgit\jiejian\README.md` 打开 markdown 文档
* `D:\code\atomgit\jiejian\shortcuts\File Explorer.lnk` 打开快捷方式
* `xg/xigua` 打开西瓜视频

### 打开搜索

* `by` 必应搜索：在框中输入 by[空格?]{关键字}
* `bi` 哔哩哔哩搜索
* `bd` 百度搜索
* `ip` 归属地查询
* `so` 360 搜索
* `sg` 搜狗搜索
* `wz` 无追搜索
* `xg` 西瓜视频
* `yc` 异次元软件
* `gh` Github

![](https://foruda.gitee.com/images/1689321508560702893/2457a573_426858.gif "动画.gif")

### 打开内部/外部命令

内部命令

* `zhongduan/cmd/终端` 终端
* 网络连接
* 收藏夹
* 字体
* 打印机
* 我的文档
* 回收站
* 我的桌面
* 我的下载
* 我的图片
* 我的视频
* 我的音乐
* 环境变量
* 重启
* 关机
* 锁屏
* 睡眠
* 激活屏幕保护程序
* 清空回收站
* 息屏
* 注销
* 静音
* 上一曲
* 下一曲
* 暂停
* 息屏
* 睡眠
* 关机

### 增强的操作文件和网址的能力

当在 anyrun 编辑框中输入文件路径/网址可以获得以下能力：

* 打开网址
* 打开文件
* 前往文件夹
* 查看属性
* 打印文件
* 删除文件
* 在 Bash 中打开（若 data.csv 中 d 列 `Bash` 对应的 b 列路径存即启用）
* 在终端中打开（若 data.csv 中 d 列 `新终端` 对应的 b 列路径存即启用新终端，否则启用旧终端 cmd.exe）
* 在 VSCode 中打开（若 data.csv 中 d 列 `VSCode` 对应的 b 列路径存即启用）
* 在 IDEA 中打开（若 data.csv 中 d 列 `IDEA` 对应的 b 列路径存即启用）

## 8. 热串 之 直达网址（Z 直达模式）

为避免误触，排除了在文本编辑器/ftp/git/IDE/office/sql/窗口软件中激活 z 模式。

* zbd 打开 百度一下
* zbi 打开 哔哩哔哩
* zdy 打开 电影天堂
* zit 打开 IT 之家
* zma 打开 QQ 邮箱
* zxg 打开 西瓜视频

zbd 示例

![](https://foruda.gitee.com/images/1689318989538537685/7b71d232_426858.gif)

zbi 示例

![](https://foruda.gitee.com/images/1689319007424875617/4934e693_426858.gif)

## 9. 热串之 扩展片段：将字符串替换为自定义话术（X 拓展模式）【可配置】

固定配置

* xnow 插入当前日期时间，举例 `2023-08-27 09:10:41`
* xdate 插入特定格式的当前日期时间，举例 `Date: 2023-12-22 21:23:30`

data.csv 可自由配置

* xnb 很牛呀
* xnm 你妹的
  
![](https://foruda.gitee.com/images/1689259764657007580/13e4cb48_426858.gif "动画.gif")

* xwx 😄 微笑
* xlh 😊 脸红
* xok 👌 OK
* xax ❤️ 爱心
* xbz 📰 报纸
* xbq 🏷️ 标签
* xsq 🔖 书签
* xsh 💩 大便

![](https://foruda.gitee.com/images/1689259802922906219/d546cc12_426858.gif "动画.gif")

## 10. 左键辅助

受到 quicker 影响，试验性的加入左键辅助功能。

在鼠标左键按下的同时按下 a 键时，若选中为网址则打开网址，否则百度搜索选中内容。

## 11. CapsLock 模式

在按下 CapsLock 键的同时再按下以下键，其中大部分按键习惯同 [MyKeymap][]。

| 按键 | 用途 |
| ---- | ---- |
| Q | 最大化或还原程序 |
| W | 切换到上个窗口 |
| E | 当前窗口恢复不透明 |
| R | 在当前程序的窗口轮换 |
| T | 当前窗口调成半透明(Translucent) |
| Y | 切换到上一个虚拟桌面 |
| P | 切换到下一个虚拟桌面 |
| Z | 复制文件路径或纯文本 |
| X | 关闭窗口 |
| V | 窗口移到下一个显示器 |
| B | 窗口最小化 |
| 空格 | 复制选中文件路径并打开 Anyrun 启动器 |

## 12. 双击模式

预设动作如下，若 2 次双击小于 239 毫秒则触发

* 双击 Alt：息屏（无提醒）
* 双击 Home：睡眠（无提醒）
* 双击 End：关机（有确认对话框）

可以手动去 data.csv 中进行修改。
由于中英文切换经常用到 Shift，日常快捷键常用到 ctrl，所以这两个按键不再提供作为前置键。
d 列置空 或者 删除该列可以屏蔽该命令。目前仅支持上述第 7 小节列出的所有内部命令。

## 13. 自定义配置说明

配置文件 **app.csv**（用于配置软件的快捷键）、**data.csv**（用于配置启动器候选项以及热键、热串） 必须和 ahk 脚本文件在同一级目录，且必须使用 GB18030 字符集。

app.csv 使用了正则表达式，需要使用者对正则有一定了解。主要用到了 `^$` 锚和不区分大小写 `i` 选项。

推荐使用 [LiberOffice](https://www.libreoffice.org/download/download-libreoffice/) 或微软 Office 打开。不要使用 WPS 进行打开，WPS 目前有兼容性问题。

### app.csv 配置

用于增强和改写快捷键

* C 列（标识符）为必填项。其余皆为可选项。只需填写需要变更的快捷键即可，否则可留空。
* F 列可留空，否则默认触发关闭窗口动作。

其他列可参考已有写法。

* M 列 是否启用列，默认启用，不启用请填写 n 或者 N。

### data.csv 配置

anyrun 启动器用

* A列 类型：app / web
* B列 启动路径：实际运行的网址或程序路径
* D列 运行名称：用于展示
* E列 运行关键字：匹配关键字，若有多个通过 | 进行分割。

自定义热键

* A列 类型：app / web / file / text
* B列 启动路径：实际运行的网址或程序路径
* F列 热键关键字：例如 !6 表示 Ctrl + 数字 6

自定义热串

* A列 类型：app / web / file / text
* B列 启动路径：实际运行的网址或程序路径
* C列 要激活的窗口(仅app 热串用)
* G列 热串关键字：例如 zbd 表示打开百度网

其他列可参考已有写法。

* H列 是否启用列，默认启用，不启用请填写 n 或者 N。
* I列 备注列，改列不会被解析

## 未来计划

* 支持 Arc 浏览器 for windows
* icon 点击后使用新图标，而不是系统样式的图标
* 配置文件不太易用，需要优化

## 常见问题

### 如何将捷键设置为开机自启

在系统栏中找到捷键，勾选开机自启即可。

也可手动开启。运行 `shell:startup`，根据自己是 32 还是 64 位系统，按住 Alt 键后将 `jiejian32.exe` 或 `jiejian64.exe` 拖入 Startup 文件夹内即可。

### 启动 zeal.exe 发现加载目录为空

使用 zeal.lnk 这种形式进行启动。汽水音乐使用 `C:\Users\zhangsan\AppData\Local\Programs\Soda Music\SodaMusic.lnk` 也是同理，因为直接用 exe 也起不来。

## 附录

### 软件搭配玩法

可搭配 WGestures 2

由于 WGestures 1 由于不支持字母手势。这里我选用的是【付费】【win mac】[WGestures 2][WGestures 2付费链接]。它是一款跨平台全局鼠标手势，且完美契合本软件。目前售价 35 米，优惠的[购买地址][WGestures 2付费链接]我也放这儿了。

### 已适配软件

软件入选原则：主要收录热门软件，其中主要以浏览器比较全。

支持但不限于以下百余款软件，且持续更新中...

* 【压缩】360 压缩 4.0【部分支持】
* 【压缩】7zip 24.01
* 【压缩】Bandizip 7.32
* 【压缩】WinRAR 6.24
* 【压缩】WinZip
* 【压缩】好压【部分支持】
* 【系统】Win 7、10、11 资源管理器
* 【系统】Win 7、10 记事本
* 【系统】Win 11 新版记事本
* 【系统】Win 7 桌面
* 【系统】Win 10 桌面
* 【系统】Win 10、11 设置
* 【浏览器】115、123
* 【浏览器】2345
* 【浏览器】360 AI
* 【浏览器】360 极速
* 【浏览器】360 游戏
* 【浏览器】360 安全
* 【浏览器】Avast 浏览器
* 【浏览器】Brave 浏览器
* 【浏览器】CCleaner Browser
* 【浏览器】Chrome 谷歌 & 百分 & 小马
* 【浏览器】Duck 浏览器
* 【浏览器】Duoyu 多御
* 【浏览器】Edge
* 【浏览器】Firefox火狐 & Tor洋葱 & Waterfox
* 【浏览器】Opera
* 【浏览器】QQ
* 【浏览器】UC
* 【浏览器】UU
* 【浏览器】Vivaldi
* 【浏览器】Yandex
* 【浏览器】傲游
* 【浏览器】斑斓石
* 【浏览器】飞牛
* 【浏览器】华为
* 【浏览器】极速
* 【浏览器】联想
* 【浏览器】猎豹
* 【浏览器】猫眼
* 【浏览器】猎鹰
* 【浏览器】蚂蚁
* 【浏览器】青鸟
* 【浏览器】搜狗
* 【浏览器】双核
* 【浏览器】星愿
* 【浏览器】想天
* 【浏览器】小K、小白、小智
* 【浏览器】一点
* 【浏览器】微软 IE 11【已过时】
* 【浏览器】红芯【已过时】
* 【音乐类】Foobar2000
* 【音乐类】iTunes
* 【音乐类】MusicBee
* 【音乐类】MusicPlayer2
* 【音乐类】Winamp 5.9.2
* 【音乐类】洛雪音乐助手
* 【音乐类】QQ 音乐
* 【音乐类】Spotify
* 【音乐类】方格音乐【部分支持】
* 【音乐类】酷我音乐
* 【音乐类】汽水音乐
* 【音乐类】网易云音乐
* 【音乐类】喜马拉雅
* 【音乐类】酷狗音乐
* 【视频类】GridPlayer【部分支持】
* 【视频类】KMPlayer 64位
* 【视频类】mpv【部分支持】
* 【视频类】PotPlayer 64位
* 【视频类】vlc
* 【视频类】暴风影音 5
* 【视频类】恒星播放器
* 【视频类】迅雷影音
* 【视频类】影音先锋
* 【视频类】荐片播放器
* 【视频类】哔哩哔哩
* 【视频类】爱奇艺
* 【视频类】优酷
* 【视频类】腾讯视频
* 【视频类】斗鱼直播【部分支持】
* 【sql】Beekeeper Studio
* 【sql】Heidisql
* 【sql】Navicat
* 【sql】SQLyog
* 【markdown】MarkdownPad2
* 【markdown】MarkText
* 【markdown】Typora
* 【editor】Bracket
* 【editor】CudaText
* 【editor】Editplus
* 【editor】EmEditor
* 【editor】Everedit
* 【editor】Fleet
* 【editor】Geany
* 【editor】Kate
* 【editor】Notepad++
* 【editor】NotepadNext
* 【editor】Notepad--
* 【editor】Notepads
* 【editor】Notepad2
* 【editor】Notepad3
* 【editor】SciTE
* 【editor】skylark
* 【editor】Sublime
* 【editor】Ultraedit
* 【editor】Atom【已过时】
* 【file compare】Beyond Compare
* 【file compare】WinMerge
* 【IDE】DevC++
* 【IDE】Eclipse
* 【IDE】HbuilderX
* 【IDE】Aqua、Clion、Datagrip、Dataspell、Goland、Idea、Pycharm、Phpstorm、Rider、RubyMine、RustRover、Webstorm、Writerside
* 【IDE】Android Studio、华为 DevEco Studio
* 【IDE】MyEclipse
* 【IDE】Rstudio
* 【IDE】SpringToolSuite4
* 【IDE】VS Code
* 【IDE】Visual Studio
* 【IDE】Netbean 32 位 & Jmeter
* 【IDE】Netbean 64 位
* 【http调试】Apifox
* 【http调试】ApiPost
* 【http调试】HTTPie
* 【http调试】Postman
* Zeal
* 【git】GitHub 桌面版
* 【git】GitKraken
* 【git】SourceTree
* 【git】小乌龟 git 合并程序
* 【终端类】Bitvise SSH Client
* 【终端类】Finalshell
* 【终端类】Hyper
* 【终端类】MobaXterm
* 【终端类】SecureCRT
* 【终端类】Tabby
* 【终端类】Termius
* 【终端类】WindTerm
* 【终端类】WindowsTerminal
* 【终端类】Xshell
* 【终端类】zoc
* 【ftp】Filezilla
* 【ftp】FlashFXP
* 【ftp】Xftp
* 【pdf】Adobe Acrobat
* 【pdf】Right PDF Reader
* 【pdf】Sumatra PDF 主页
* 【pdf】UPDF
* 【pdf】福昕 PDF 编辑器
* 【pdf】福昕阅读器
* 【pdf】极速 PDF
* 【pdf】金山 PDF 独立版
* 【pdf】迅读 PDF
* 【pdf】永中 Office 版式阅读器
* 【pdf】可牛 PDF
* 【pdf】万兴 PDF 阅读器
* 【office】LibreOffice
* 【office】微软 Excel 2007、2021
* 【office】微软 PPT 2007、2021
* 【office】微软 Word 2007、2021
* 【office】ONLYOFFICE
* 【office】WPS Office
* 【office】永中简报 2024
* 【office】永中表格 2024
* 【office】永中文字 2024
* 【看图】2345 看图王
* 【看图】FSViewer
* 【看图】Honeyview or BandiView
* 【看图】ImageGlass
* 【看图】JPEGView
* 【看图】pineapple pictures 菠萝看图
* 【看图】Windows 照片查看器
* 【看图】WPS 图片查看器
* 【看图】xnview
* Motrix
* Snipaste
* Thunderbird 雷鸟
* 阿里云客户端
* 稻壳阅读器
* 炉石传说
* 腾讯 QQ
* 微信

不支持的软件：

* 抖音
* 快压

无需适配的软件：

* 西瓜视频

### 已知 bug

* 适配不太好的软件：Right PDF Reader 的鼠标侧边后退键无效。
* Caps + x / ctrl + f4 关闭窗口对一些软件不奏效：极客卸载、注册表编辑器、windows 任务管理器（似乎屏蔽了 Ctrl 键）、高级系统设置等窗口、WGestures 的导入导出窗口。
* 当任务管理器或者高级系统设置等窗口激活时鼠标滚轮捕捉不到，会导致鼠标靠在边界调节音量功能失效

### 软件升级

jiejian.exe 的文件版本为当前四位版本号，产品版本为当前编译的 ahk 版本。

升级信息会写入了注册表，而非传统 ini 文件。

升级有两个渠道，release 为正式版 和 snapshot 为测试版。release 版本中会自动检查更新间隔为 1 天。snapshot 版本中为每次启动的时候。

下载新的发布包，提取 jiejian.exe / jiejian64.exe 覆盖即可。

另外 app.csv 和 data.csv 可按需覆盖。一般情况下建议 app.csv 和 data.csv 自定义内容追加在尾部，方便迁移数据。

## 写在最后

本程序编辑 csv 文件用到了开源软件 LiberOffice。主要发布平台为 [GitHub](https://github.com) 和 [AtomGit](https://atomgit.com)。

由于 autohotkey 这门语言容易学习。再加上我之前编程功底，从设计到开发均由我一人完成。部分功能以及代码参考自 Windows 软件 [MyKeymap][] | [源码][MyKeymap Github repo]、[Capslock+][]，并从 Win 软件 [Quicker][]、[uTools][] 以及 macOS 软件 [BetterAndBetter][]、[HapiGo][] 和 [Raycast][] 中受到启发。在此默默感谢。

  [MyKeymap]: https://xianyukang.com/MyKeymap.html '很多代码都参考借鉴了它'
  [MyKeymap Github repo]: https://github.com/xianyukang/MyKeymap "感谢作者"
  [WGestures 1]: https://www.yingdev.com/projects/wgestures '一款很屌的免费鼠标手势'
  [WGestures 2]: https://www.yingdev.com/projects/wgestures2 "一款很屌的付费鼠标手势"
  [WGestures 2付费链接]: https://store.lizhi.io/site/products/id/523?cid=46jjayiu "一款很屌的付费鼠标手势"
  [Capslock+]: https://capslox.com/capslock-plus ""
  [Raycast]: https://www.raycast.com ""
  [HapiGo]: https://www.hapigo.com ""
  [BetterAndBetter]: https://www.better365.cn/bab2.html ""
  [Quicker]: https://getquicker.net/ ""
  [uTools]: https://www.u.tools/ ""

  [捷键]: https://atomgit.com/acc8226/jiejian/tags?tab=release "我的诚意之作" 

  [bilibili 哔哩哔哩]: https://www.bilibili.com/ '哔哩哔哩'
  [汽水音乐]: https://www.douyin.com/qishui '汽水音乐'
